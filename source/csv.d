import core.time;

import std.stdio;

import elevator;

class CsvWriter {
private:
	File file;
	Duration total_time;

public:
	this(string filename) {
		file = File(filename, "w");
	}

	~this() {
		file.close();
	}

	void write(Duration step_length, Elevator[] elevators) {
		total_time += step_length;
		file.write(cast(double) (total_time.total!"msecs") / 1000.0, "\t");
		foreach (i, e; elevators) {
			file.write(e.getCurrent);
			if (i != elevators.length - 1)
				file.write("\t");
		}
		file.write("\n");
	}
}
