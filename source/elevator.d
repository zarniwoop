import core.time;
import std.algorithm : max, remove;

import building;
import stats;
import user;

enum ElevatorState {
	moving_up,
	moving_down,
	stationary,
	doors_opening,
	doors_closing,
	passengers_getting_off,
	passengers_getting_in
}

enum OperationStepsNumber {
	move_on_floor = 1,
	actuate_doors = 1,
	// One step needed to move two passengers
	passenger_movement = 1,
	passengers_number = 2
}

string toString(ElevatorState state) {
	final switch (state) {
	case ElevatorState.moving_up:
		return " /\\ ";
	case ElevatorState.moving_down:
		return " \\/ ";
	case ElevatorState.stationary:
		return " -- ";
	case ElevatorState.doors_opening:
		return "<  >";
	case ElevatorState.doors_closing:
		return " >< ";
	case ElevatorState.passengers_getting_off:
		return "<---";
	case ElevatorState.passengers_getting_in:
		return "--->";
	}
}

// Two elevators belong to the same equivalence class if their lowest and highest
// serviced stories are the same.
struct Class {
	int lowest;
	int highest;
}

interface ElevatorInterface {
	void moveTo(int story);
	void setTakeUserPred(TakeUserPred pred);
	int getCurrent();
	int getDestination();
	int[] getCalls();
	int lowest();
	int highest();
	Class clas();
	ElevatorState getState();
}

class Elevator : ElevatorInterface {
private:
	User[] passengers;
	int current;
	int destination;
	ElevatorState state;
	int current_steps; // Number of steps done for the current operation
	int total_steps; // Total number of steps required for the current operation
	int lowest_story;  // Lowest serviced story
	int highest_story; // Highest
	TakeUserPred takeUserPred;

	invariant {
		assert(lowest_story <= current && current <= highest_story);
	}

public:

	this(int lowest, int highest) {
		lowest_story = lowest;
		highest_story = highest;
		current = lowest;
		state = ElevatorState.stationary;
	}

	ulong nbPassengers() {
		return passengers.length;
	}

	void moveTo(int story)
	in (state == ElevatorState.stationary) {
		destination = story;
		state = destination > current ? ElevatorState.moving_up : ElevatorState.moving_down;
	}

	int getCurrent() {
		return current;
	}

	int getDestination() {
		return destination;
	}

	int[] getCalls() {
		return passengers.destinations;
	}

	ElevatorState getState() {
		return state;
	}

	int highest() const { return highest_story; }
	int lowest() const { return lowest_story; }

	void step(Duration step_length, Story story, Stats stats) {
		// Step passengers
		foreach (p; passengers) {
			p.addJourneyTime(step_length);
		}

		// Change state
		current_steps++;
		if (current_steps >= total_steps) {
			current_steps = 0;

			final switch (state) {
			case ElevatorState.moving_up:
			case ElevatorState.moving_down:
				current += state == ElevatorState.moving_up ? 1 : -1;
				if (current == destination) {
					state = ElevatorState.doors_opening;
					total_steps = OperationStepsNumber.actuate_doors;
				}
				break;

			case ElevatorState.doors_opening:
				state = ElevatorState.passengers_getting_off;
				total_steps = OperationStepsNumber.passenger_movement;
				break;

			case ElevatorState.doors_closing:
				state = ElevatorState.stationary;
				total_steps = 0;
				break;

			// Passengers movements
			case ElevatorState.passengers_getting_off:
				for (int i = cast(int) passengers.length - 1; i >= 0; i--) {
					if (passengers[i].getDestination == current) {
						stats.addPassenger(passengers[i]);
						passengers = passengers.remove(i);
					}
				}
				state = ElevatorState.passengers_getting_in;
				break;

			case ElevatorState.passengers_getting_in:
				passengers ~= story.takeUsers(takeUserPred);
				state = ElevatorState.doors_closing;
				total_steps = OperationStepsNumber.actuate_doors;
				break;

			case ElevatorState.stationary:
				break;
			}
		}
	}

	TakeUserPred getTakeUserPred() {
		return takeUserPred;
	}

	void setTakeUserPred(TakeUserPred pred) {
		takeUserPred = pred;
	}

	Class clas() const {
		return Class(lowest, highest);
	}
}
