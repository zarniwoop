import core.time;

import elevator;

enum Direction { up, down }

class User {
private:
	Duration waiting_time;
	Duration journey_time;

public:
	immutable int destination;
	immutable int origin; // Origin story

	Class clas;

	this(int origin, int destination) {
		this.origin = origin;
		this.destination = destination;
	}

	int getDestination() {
		return destination;
	}

	Direction twoButtonsCall() {
		return (destination > origin) ? Direction.up : Direction.down;
	}

	void addWaitingTime(Duration length) {
		waiting_time += length;
	}

	void addJourneyTime(Duration length) {
		journey_time += length;
	}

	Duration waitingTime() {
		return waiting_time;
	}

	Duration journeyTime() {
		return journey_time;
	}
}

int[] destinations(User[] users) {
	int[] dests;
	foreach (u; users) {
		dests ~= u.getDestination;
	}
	return dests;
}

bool[2] twoButtonCalls(User[] users, Class clas) {
	bool[2] calls = [false, false];
	foreach (u; users) {
		if (u.clas == clas) {
			if (u.destination > u.origin)
				calls[Direction.up] = true;
			else if (u.destination < u.origin)
				calls[Direction.down] = true;
		}
	}
	return calls;
}
