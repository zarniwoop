import std.datetime;
import std.getopt;
import std.stdio;

import sdlang;

import algorithm;
import building;
import csv;
import elevator;
import people;

Building parseSDL(string name) {
	Tag root = parseFile(name);
	int stories = root.expectTagValue!int("stories");
	Elevator[] elevators;
	foreach (tag; root.tags["elevator"]) {
		elevators ~= new Elevator(tag.values[0].get!int, tag.values[1].get!int);
	}

	return new Building(stories, elevators);
}

void main(string[] args) {
	// Options
	string filename = "building.sdl";
	string csv_filename = "";
	ulong steps_number = 100000;
	ulong step_length_msecs = 500;
	AlgorithmEnum algo_enum = AlgorithmEnum.elevator_better;
	GetoptResult help_info = getopt(args,
		"file", "Building file to read.", &filename,
		"out", "CSV file to write to.", &csv_filename,
		"steps", "Number of steps to simulate.", &steps_number,
		"step_length", "Length of a step in milliseconds.", &step_length_msecs,
		"algorithm", "Algorithm to use.", &algo_enum);
	if (help_info.helpWanted) {
		defaultGetoptPrinter("Zarniwoop: Elevator Simulator", help_info.options);
		return;
	}

	Duration step_length = msecs(step_length_msecs);

	// Create a building
	Building build = parseSDL(filename);

	// Create a CSV writer
	if (csv_filename != "") {
		build.setCsvWriter(new CsvWriter(csv_filename));
	}

	// Create a people generator
	People people = new SimplePeople(build.storyPeopleInterfaces);

	// Create an algorithm object
	Algorithm algo;
	final switch (algo_enum) {
	case AlgorithmEnum.elevator:
		algo = new ElevatorAlgorithm(build.storyInterfaces, build.elevatorInterfaces);
		break;
	case AlgorithmEnum.elevator_better:
		algo = new ElevatorAlgorithmBetter(build.storyInterfaces, build.elevatorInterfaces);
		break;
	}

	// Create a simple building with 10 stories and one elevator
	/*Elevator[] elevators = new Elevator[1];
	elevators[0] = new Elevator(0, 19, stats);*/
	/*Elevator[2] elevators;
	elevators[0] = new Elevator(0, 19);
	elevators[1] = new Elevator(0, 19);
	Building build = new Building(20, elevators);*/

	// Main loop
	ulong i = 0;
	while (i < steps_number) {
		// Step all existing people
		build.step(step_length);

		// Add new people
		people.addUsers(.5);

		// If any elevator is stationary, run the algorithm to decide what to do next
		if (build.elevatorWaiting) {
			algo.move;
		}

		//writeln(build.convToString);
		//Thread.sleep(msecs(500));
		//writefln("Step %d", i);
		//writeln(build.convToString);
		i++;
	}
	writeln(build.convToString);
	writefln("Simulation time: %s", steps_number*step_length);
	writeln(build.getStats);
}
