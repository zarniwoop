import core.time;
import std.algorithm;
import std.conv;
import std.format : format;
import std.range;
import std.stdio;

import algorithm;
import csv;
import elevator;
import user;
import stats;

class Building {
private:
	Elevator[] elevators;
	Story[] stories;
	Stats stats;
	CsvWriter csv_writer;

public:
	this(int height, Elevator[] elevators) {
		this.elevators = elevators.sort!((a, b) => a.highest - a.lowest < b.highest - b.lowest).release;

		stories = new Story[height];
		foreach (i, ref s; stories)
			s = new Story(this);

		stats = new Stats;
	}

	void setCsvWriter(CsvWriter csv_writer) {
		this.csv_writer = csv_writer;
	}

	ElevatorInterface[] elevatorInterfaces() {
		return to!(ElevatorInterface[])(elevators);
	}

	StoryInterface[] storyInterfaces() {
		return to!(StoryInterface[])(stories);
	}

	StoryPeopleInterface[] storyPeopleInterfaces() {
		return to!(StoryPeopleInterface[])(stories);
	}

	string convToString() {
		string res = "";
		foreach_reverse (i, story; stories) {
			res ~= format!"%3d %6d"(i, story.nbUsers);
			foreach (e; elevators) {
				if (e.getCurrent == i) {
					res ~= format!"%4d %s (%3d)\t"(e.nbPassengers, e.getState.toString, e.getDestination);
				} else if (e.lowest <= i && i <= e.highest) {
					res ~= "      ||       \t";
				} else {
					res ~= "               \t";
				}
			}
			res ~= "\n";
		}
		return res;
	}

	void step(Duration step_length) {
		foreach (s; stories) {
			s.step(step_length);
		}
		foreach (e; elevators) {
			e.step(step_length, stories[e.getCurrent], stats);
		}

		if (csv_writer !is null) {
			csv_writer.write(step_length, elevators);
		}
	}

	Stats getStats() {
		return stats;
	}

	ulong height() {
		return stories.length;
	}

	bool elevatorWaiting() {
		return any!((Elevator a) => a.getState == ElevatorState.stationary)(elevators);
	}

	Class computeClass(int origin, int destination) const {
		// This function is why the order of elevators matters, see constructor.
		// min and max
		immutable int m = origin < destination ? origin : destination;
		immutable int M = origin < destination ? destination : origin;
		int i = 0;
		while (elevators[i].lowest > m || elevators[i].highest < M)
			i++;
		return elevators[i].clas;
	}
}

interface StoryInterface {
	int[] getDestinations();
	bool[2] getTwoButtonsCall(Class clas);
	bool getCall();
}

interface StoryPeopleInterface {
	void addUser(User user);
}

alias TakeUserPred = bool delegate(User);

class Story : StoryInterface, StoryPeopleInterface {
private:
	User[] users;
	const Building build;

public:
	this(const Building build) {
		this.build = build;
	}

	int[] getDestinations() {
		return users.destinations;
	}

	bool[2] getTwoButtonsCall(Class clas) {
		return users.twoButtonCalls(clas);
	}

	bool getCall() {
		return users.length > 0;
	}

	void addUser(User user) {
		user.clas = build.computeClass(user.origin, user.destination);
		users ~= user;
	}

	User[] takeUsers(TakeUserPred pred) {
		User[] res = array(users.filter!pred());
		users = users.remove!(a => pred(a), SwapStrategy.unstable)();
		return res;
	}

	ulong nbUsers() {
		return users.length;
	}

	void step(Duration step_length) {
		foreach (u; users) {
			u.addWaitingTime(step_length);
		}
	}
}
