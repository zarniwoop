module people.simple;

import std.random : uniform;

import building;
import people;
import user;

class SimplePeople : People {
private:
	StoryPeopleInterface[] stories;

public:
	this(StoryPeopleInterface[] stories) {
		this.stories = stories;
	}

	bool addUsers(double step_length) {
		double prob = step_length / 2.0; // Add a person every 2 seconds
		if (uniform(0.0, 1.0) > prob) {
			return false;
		}

		int origin = uniform(0, cast(int) stories.length);
		int destination = uniform(0, cast(int) stories.length - 1);
		if (destination >= origin)
			destination++;
		User user = new User(origin, destination);
		stories[origin].addUser(user);
		return true;
	}
}
