module algorithm.elevator;

import algorithm;
import building;
import elevator;
import user;

// The Elevator Algorithm

class ElevatorAlgorithm : Algorithm {
private:
	ElevatorInterface[] elevators;
	StoryInterface[] stories;
	Direction[] directions;

public:
	this(StoryInterface[] stories, ElevatorInterface[] elevators) {
		this.stories = stories;
		this.elevators = elevators;
		directions = new Direction[elevators.length];
		foreach (e; elevators) {
			e.setTakeUserPred(a => a.clas == e.clas);
		}
	}

	void move() {
		foreach (i, e; elevators) {
			if (e.getState != ElevatorState.stationary)
				continue;
			// Find the next story where to go
			// First, find the closest call up and down inside the elevator.
			int[] in_calls = e.getCalls;
			int highest = e.lowest - 1, lowest = e.highest + 1;
			foreach (call; in_calls) {
				if (call < e.getCurrent && call > highest)
					highest = call;
				if (call > e.getCurrent && call < lowest)
					lowest = call;
			}

			// Try to find a closer external call
			int story = e.getCurrent - 1;
			while (story > highest && !stories[story].getCall)
				story -= 1;
			if (story > highest)
				highest = story;

			story = e.getCurrent + 1;
			while (story < lowest && !stories[story].getCall)
				story += 1;
			if (story < lowest)
				lowest = story;

			if (lowest <= e.highest && (directions[i] == Direction.up || highest == e.lowest - 1)) {
				directions[i] = Direction.up;
				e.moveTo(lowest);
			} else if (highest >= e.lowest && (directions[i] == Direction.down || lowest == e.highest + 1)) {
				directions[i] = Direction.down;
				e.moveTo(highest);
			}

		}
	}
}
