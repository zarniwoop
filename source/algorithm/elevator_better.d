module algorithm.elevator_better;

import algorithm;
import building;
import elevator;
import user;

// The Elevator Algorithm, a little more intelligent

class ElevatorAlgorithmBetter : Algorithm {
private:
	ElevatorInterface[] elevators;
	StoryInterface[] stories;
	Direction[] directions;

public:
	this(StoryInterface[] stories, ElevatorInterface[] elevators) {
		this.stories = stories;
		this.elevators = elevators;
		directions = new Direction[elevators.length];
		foreach (e; elevators) {
			e.setTakeUserPred(a => a.clas == e.clas);
		}
	}

	void move() {
		// Check that a given story is not already the destination of an elevator
		// Returns false if not assigned
		bool checkAlreadyAssigned(int story, Class clas) {
			foreach (e; elevators) {
				if (e.getDestination == story && e.clas == clas)
					return true;
			}
			return false;
		}

		foreach (i, e; elevators) {
			if (e.getState != ElevatorState.stationary)
				continue;
			// Find the next story where to go
			// First, find the closest call up and down inside the elevator.
			int[] in_calls = e.getCalls;
			int[2] best;
			best[Direction.up] = e.highest + 1;
			best[Direction.down] = e.lowest - 1;

			foreach (call; in_calls) {
				if (call < e.getCurrent && call > best[Direction.down])
					best[Direction.down] = call;
				if (call > e.getCurrent && call < best[Direction.up])
					best[Direction.up] = call;
			}

			// Try to find a closer external call
			immutable Class clas = e.clas;

			alias checkFound = () => directions[i] == Direction.up ? best[Direction.up] <= e.highest : best[Direction.down] >= e.lowest;

			void findExternalCall() {
				immutable int increment = directions[i] == Direction.up ? 1 : - 1;
				alias pred = story => directions[i] == Direction.up ? story < best[Direction.up] : story > best[Direction.down];

				void findInDirection(Direction dir) {
					int story = e.getCurrent + increment;
					while (pred(story) && (!stories[story].getTwoButtonsCall(clas)[dir]
							|| checkAlreadyAssigned(story, clas)))
						story += increment;
					if (pred(story))
						best[directions[i]] = story;
				}

				// Try to find somebody in the same direction who wants to go in the same direction
				findInDirection(directions[i]);
				// If unsuccessful, and if there no internal call, try to find somebody
				// in the same direction who wants to go in the reverse direction
				if (!checkFound())
					findInDirection(cast(Direction) 1 - directions[i]);
			}

			// First, in the current direction
			findExternalCall();
			if (!checkFound()) {
				// Else, reverse the direction and try again
				directions[i] = cast(Direction) 1 - directions[i];
				findExternalCall();
			}

			if (checkFound()) {
				e.moveTo(best[directions[i]]);
			}
		}
	}
}
