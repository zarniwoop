module algorithm;

public import algorithm.elevator;
public import algorithm.elevator_better;

import building;

enum AlgorithmEnum {
	elevator,
	elevator_better
};

interface Algorithm {
	void move();
}
