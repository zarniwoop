import core.time;
import std.format : format;

import user;

class Stats {
private:
	Duration total_journey_time;
	Duration total_waiting_time;
	ulong total_passengers;

public:
	void addPassenger(User user) {
		total_journey_time += user.journeyTime;
		total_waiting_time += user.waitingTime;
		total_passengers++;
	}

	override string toString() {
		return format!"Average waiting time: %s\nAverage journey time: %s\nAverage total time: %s\nTotal passengers transported: %d"
			(total_journey_time / cast(double) total_passengers, total_waiting_time / cast(double) total_passengers,
			(total_journey_time + total_waiting_time) / cast(double) total_passengers,
			total_passengers);
	}
}
